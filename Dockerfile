FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY . /app
RUN mvn install

EXPOSE 9090
CMD ["mvn", "tomcat7:run"]
